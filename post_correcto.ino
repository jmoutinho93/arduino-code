#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

// Datos de la red de internet
const char* ssid     = "ASUS";
const char* password = "qwer1234";

// defines pins numbers de los sensores ultrasonicos
// sensor 1
const int trigPin = 2;  //D4
const int echoPin = 0;  //D3
//sensor 2
const int trigPinS2 = 4;  //D2
const int echoPinS2 = 5;  //D1

// defines variables
// sensor 1
long duration;
int distance;
//sensor 2
long durationS2;
int distanceS2;

void setup() {
  // Put your setup code here, to run once:
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(trigPinS2, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPinS2, INPUT); // Sets the echoPin as an Input
  Serial.begin(115200); // Starts the serial communication
  delay(10);

  // Conexion a Wifi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Conectando a:\t");
  Serial.println(ssid); 
 
  // Esperar a que nos conectemos
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(200);
    Serial.print('.');
  }
 
  // Mostrar mensaje de exito y dirección IP asignada
  Serial.println();
  Serial.print("Conectado a:\t");
  Serial.println(WiFi.SSID()); 
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());
}

void loop() {
 
  //codigo del sensor 1 con su POST
  
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  
  // Calculating the distance
  distance= duration*0.034/2;
  // Prints the distance on the Serial Monitor
  Serial.print("Distancia del sensor 1: ");
  Serial.println(distance);
  delay(2000); 

  if (distance > 50){
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
  const size_t capacity = JSON_OBJECT_SIZE(2) + 20;
  DynamicJsonBuffer jsonBuffer(capacity);
  const char* json = "{\"id\":1,\"status\":0}";
  JsonObject& root = jsonBuffer.parseObject(json);
  int id = root["id"]; // 1
  int status = root["status"]; // 0  
  HTTPClient http;    //Declare object of class HTTPClient
  http.begin("http://192.168.1.13:1234/api/v1/insert");      //Specify request destination
  http.addHeader("Content-Type", "application/json");  //Specify content-type header
  int httpCode = http.POST(json);   //Send the request
  String payload = http.getString();                                        //Get the response payload
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
  http.end();  //Close connection 
  } else { 
  Serial.println("Error in WiFi connection"); 
  }
  delay(20000);
  }
  else {
   if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
  const size_t capacity = JSON_OBJECT_SIZE(2) + 20;
  DynamicJsonBuffer jsonBuffer(capacity);
  const char* json = "{\"id\":1,\"status\":1}";
  JsonObject& root = jsonBuffer.parseObject(json);
  int id = root["id"]; // 1
  int status = root["status"]; // 0  
  HTTPClient http;    //Declare object of class HTTPClient
  http.begin("http://192.168.1.13:1234/api/v1/insert");      //Specify request destination
  http.addHeader("Content-Type", "application/json");  //Specify content-type header
  int httpCode = http.POST(json);   //Send the request
  String payload = http.getString();                                        //Get the response payload
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
  http.end();  //Close connection 
  } else { 
  Serial.println("Error in WiFi connection"); 
  }
  delay(20000); 
  }

  //fin del codigo del sensor 1

  //codigo del sensor 2 con su POST
  
  // Clears the trigPin
  digitalWrite(trigPinS2, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPinS2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinS2, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  durationS2 = pulseIn(echoPinS2, HIGH);
  
  // Calculating the distance
  distanceS2= durationS2*0.034/2;
  // Prints the distance on the Serial Monitor
  Serial.print("Distance del sensor 2: ");
  Serial.println(distanceS2);
  delay(2000); 

  if (distanceS2 > 50){
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
  const size_t capacity = JSON_OBJECT_SIZE(2) + 20;
  DynamicJsonBuffer jsonBuffer(capacity);
  const char* json = "{\"id\":2,\"status\":0}";
  JsonObject& root = jsonBuffer.parseObject(json);
  int id = root["id"]; // 1
  int status = root["status"]; // 0  
  HTTPClient http;    //Declare object of class HTTPClient
  http.begin("http://192.168.1.13:1234/api/v1/insert");      //Specify request destination
  http.addHeader("Content-Type", "application/json");  //Specify content-type header
  int httpCode = http.POST(json);   //Send the request
  String payload = http.getString();                                        //Get the response payload
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
  http.end();  //Close connection 
  } else { 
  Serial.println("Error in WiFi connection"); 
  }
  delay(20000);
  }
  else {
   if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
  const size_t capacity = JSON_OBJECT_SIZE(2) + 20;
  DynamicJsonBuffer jsonBuffer(capacity);
  const char* json = "{\"id\":2,\"status\":1}";
  JsonObject& root = jsonBuffer.parseObject(json);
  int id = root["id"]; // 1
  int status = root["status"]; // 0  
  HTTPClient http;    //Declare object of class HTTPClient
  http.begin("http://192.168.1.13:1234/api/v1/insert");      //Specify request destination
  http.addHeader("Content-Type", "application/json");  //Specify content-type header
  int httpCode = http.POST(json);   //Send the request
  String payload = http.getString();                                        //Get the response payload
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
  http.end();  //Close connection 
  } else { 
  Serial.println("Error in WiFi connection"); 
  }
  delay(20000); 
  }

  //fin del codigo del sensor 2

  
}
